﻿using UnityEngine;

public class DestroyProjectile : MonoBehaviour
{
    // Attaced to projectiles
    private void OnCollisionEnter(Collision collision)
    {
        // When projectile collides with enemies or the environment, they get destroyed
        if (collision.collider.tag == "Enemy" || collision.collider.tag == "Environment")
        {
            Destroy(gameObject);
        }
    }
}
