﻿using UnityEngine;

public class CameraView : MonoBehaviour
{
    // Variables that hold camera objects
    public Camera overheadCam;
    public Camera cannonCam;

    // Store transform objects
    public Transform target;
    public Transform lookTarget;

    // Used to smooth camera transitions
    public float smoothSpeed = 10.0f;
    // Stores a Vector3 for setting the camera offset
    public Vector3 offset;

    // Used to check which view is active
    private bool overheadView = false;

    private void Start()
    {
        // Calls the SwitchView function to start in the right view
        SwitchView();
    }

    // Updates at a fixed rate
    private void FixedUpdate()
    {
        // Sets the desired position of the cam equal to the targets position plus the offset value
        Vector3 desiredPosition = target.position + offset;
        // Sets the smoothed position equal to the Vector3 interpolation from the transform position to the desired position, using the smooth speed times Time.deltaTime
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        // Sets the transform position to the smoothedPosition
        transform.position = smoothedPosition;

        // Rotates the transform of the camera to look at the lookTarget position
        transform.LookAt(lookTarget.position);
    }

    public void SwitchView ()
    {
        // Checks if the overhead view is true
        if (overheadView)
        {
            // Disables the overhead view
            overheadCam.enabled = false;
            // Enables the cannon view
            cannonCam.enabled = true;

            // Sets the overhead view to false
            overheadView = false;
        }
        // Reverses the process if overhead view is false
        else
        {
            cannonCam.enabled = false;
            overheadCam.enabled = true;

            overheadView = true;
        }
    }
}
