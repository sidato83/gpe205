﻿using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.Impl;

public class TankData : MonoBehaviour
{
    // Variables to store all the tank data
    // Made public for designers to edit in the inspector
    [Header("Score")]
    public int playerScore;
    public int pointValue;

    [Header("Health")]
    public float maxHealth;
    public float currentHealth;

    [Header("Damage")]
    public float cannonDamage;
    public float bulletDamage;

    [Header("Tank Speed")]
    public float forwardSpeed = 15f;
    public float reverseSpeed = 15f;
    public float turnSpeed = 25f;

    [Header("Weapon System Movement")]
    public float turretRotateSpeed = 25f;
    public float radarRotateSpeed = 180f;
    public float cannonElevateSpeed = 20f;
    public float machineGunRotateSpeed = 30f;
    [Range(1, 20)]
    public float cannonRecoilDistance = 20f;

    [Header("Cannon Weapon System")]
    public float cannonFireDelayTimer = 2;
    public float cannonShellForce = 150f;
    public float cannonShellTTL = 5f;
    [Range(0, 1)]
    public float cannonPositionResetDelay = 0.2f;

    [Header("Machine Gun Weapon System")]
    public float bulletForce = 300f;
    public float bulletTTL = 5f;
    [Range(0,1)]
    public float bulletFiringDelay = 0.5f;

    public void AddToScore(int scoreValue)
    {
        playerScore += scoreValue;
    }
}
