﻿using UnityEngine;

[System.Serializable]
public class Powerup
{
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float cannonDamageModifier;

    public float duration;

    public bool isPermanent;

    public void OnActivate(TankData target) 
    {
        target.forwardSpeed += speedModifier;

        target.currentHealth += healthModifier;
        target.currentHealth = Mathf.Min(target.currentHealth, target.maxHealth);

        target.maxHealth += maxHealthModifier;

        target.cannonDamage += cannonDamageModifier;
    }

    public void OnDeactivate(TankData target)
    {
        target.forwardSpeed -= speedModifier;
        target.currentHealth -= healthModifier;
        target.maxHealth -= maxHealthModifier;
        target.cannonDamage -= cannonDamageModifier;
    }
}
