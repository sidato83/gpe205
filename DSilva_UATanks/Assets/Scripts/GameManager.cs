﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    // Game manager
    private void Awake()
    {
        // Checks if the instance is null
        if (instance == null)
        {
            // Sets the instance to this instance
            instance = this;
        }
        // If the instance is !null
        else
        {
            // Displays error message
            Debug.LogError("ERROR: There can only be one GameManager.");
            // Destroys the instance
            Destroy(gameObject);
        }
    }

    // Stores a TankData array
    public TankData[] gameData;
    public AIController[] aiData;

    // Stores a GameObject array
    [Header("Game Object Array")]
    public GameObject[] currentGameObjects;

    // Displays the player score
    [Header("Score")]
    public int playerOneScore;

    // Displays health and point values for game objects
    [Header("Player Stats")]
    [SerializeField]
    private float playerOneMaxHealth;
    [SerializeField]
    private float playerOneCurrentHealth;
    [SerializeField]
    private float playerOneSpeed;

    // The following sections are used to display enemy data in the manager
    [Header("Enemy One")]
    [SerializeField]
    private float enemy1Health;
    [SerializeField]
    private int enemy1PointValue;

    [Header("Enemy Two")]
    [SerializeField]
    private float enemy2Health;
    [SerializeField]
    private int enemy2PointValue;

    [Header("Enemy Three")]
    [SerializeField]
    private float enemy3Health;
    [SerializeField]
    private int enemy3PointValue;

    [Header("Enemy Four")]
    [SerializeField]
    private float enemy4Health;
    [SerializeField]
    private int enemy4PointValue;

    [Header("Enemy Five")]
    [SerializeField]
    private float enemy5Health;
    [SerializeField]
    private int enemy5PointValue;

    [Header("Enemy Six")]
    [SerializeField]
    private float enemy6Health;
    [SerializeField]
    private int enemy6PointValue;

    [Header("Enemy Seven")]
    [SerializeField]
    private float enemy7Health;
    [SerializeField]
    private int enemy7PointValue;

    // Start is called before the first frame update
    void Start()
    {
        // Called to get all point values
        GetPointValues();
    }

    // Update is called once per frame
    void Update()
    {
        // Called to update game values on display
        UpdateGameValues();
    }

    // Sets the player and enemy health values and the player score
    void UpdateGameValues()
    {
        playerOneScore = gameData[0].playerScore;
        playerOneMaxHealth = gameData[0].maxHealth;
        playerOneCurrentHealth = gameData[0].currentHealth;
        playerOneSpeed = gameData[0].forwardSpeed;
        enemy1Health = gameData[1].currentHealth;
        enemy2Health = gameData[2].currentHealth;
        enemy3Health = gameData[3].currentHealth;
        enemy4Health = gameData[4].currentHealth;
        enemy5Health = gameData[5].currentHealth;
        enemy6Health = gameData[6].currentHealth;
        enemy7Health = gameData[7].currentHealth;
    }

    // Gets the currently set point values for all enemies
    void GetPointValues()
    {
        enemy1PointValue = gameData[1].pointValue;
        enemy2PointValue = gameData[2].pointValue;
        enemy3PointValue = gameData[3].pointValue;
        enemy4PointValue = gameData[4].pointValue;
        enemy5PointValue = gameData[5].pointValue;
        enemy6PointValue = gameData[6].pointValue;
        enemy7PointValue = gameData[7].pointValue;
    }
}
