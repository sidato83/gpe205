﻿using UnityEngine;

public class PowerSpawner : MonoBehaviour
{
    public GameObject[] pickupPrefab;
    public float spawnDelay;

    private GameObject spawnedPickup;
    private float nextSpawnTime;
    private Transform tf;
    private int random;

    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();

        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        random = Random.Range(0, pickupPrefab.Length);

        if (spawnedPickup == null)
        {
            if (Time.time > nextSpawnTime)
            {
                spawnedPickup = Instantiate(pickupPrefab[random], tf.position, Quaternion.identity);
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
