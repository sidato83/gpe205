﻿using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Powerup powerup;
    public AudioClip feedback;
    [Range(0, 1)] public float feedbackVol;

    public float rotationSpeed = 100.0f;
    private Transform tf;

    private void Start()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    private void Update()
    {
        RotatePowerup();
    }

    public void OnTriggerEnter(Collider other)
    {
        PowerupController powCon = other.GetComponent<PowerupController>();

        if (powCon != null)
        {
            powCon.Add(powerup);

            if (feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, Camera.main.transform.position, feedbackVol);
            }
            Destroy(gameObject);
        }
    }

    void RotatePowerup()
    {
        float iconRotation = rotationSpeed * Time.fixedDeltaTime;
        tf.Rotate(new Vector3(0f, iconRotation, 0f));
    }
}
