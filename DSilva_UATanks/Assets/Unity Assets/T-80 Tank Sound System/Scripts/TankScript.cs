﻿// ***** THIS IS NOT MY SCRIPT, THIS WAS INCLUDED WITH THE T-80 TANK SOUND SYSTEM FROM UNITY (SoundFX Studio) *****

using UnityEngine;
using System.Collections.Generic;

namespace Complete
{
    public class TankScript : MonoBehaviour
    {
        public TankData data;
        Camera cam;

        public float max_Speed;
        public float max_TurnSpeed;

        [Range(0f, 3f)]
        public float volume_Idle = 1f, volume_Acceleration = 1f, volume_Movement = 1f, volume_Turbine = 1f, volume_Start = 1f, volume_Stop = 1f, volume_ObstacleHit = 1f;

        public AudioClip c_Idle, c_Acceleration, c_Movement, c_Turbine, c_Start, c_Stop;
        public List<AudioClip> c_ObstacleHit = new List<AudioClip>();

        private AudioSource s_Idle, s_Acceleration, s_Movement, s_Turbine, s_Start, s_Stop, s_ObstacleHit;

        private AudioSource musicSource;

        private AudioSource fadeOutSource;
        private bool fadeOut = false;
        private const float fadeOutTime = 3f;
        private float fadeOutTimer = fadeOutTime;


        public GameObject audioSourcePrefab;

        private string movementAxisName = "Vertical";
        private string turnAxisName = "Horizontal";
        private CharacterController characterController;
        private float movementInputValue = 0f;         // The current value of the movement input.
        private float prevMovementValue = 0f;
        private float turnInputValue = 0f;             // The current value of the turn input.
        private float rpm = 0f;
        private float turbineRPM = 0f;
        private float speed = 0f, normSpeed = 0f;
        private float accelerationFactor;

        private bool acceleration = false;
        private bool accelerationPrev = false;
        private bool deceleration = false;

        private Vector3 movement = Vector3.zero;

        private float timerHitSound;

        private void Awake()
        {
            cam = Camera.main;

            characterController = GetComponent<CharacterController>();
            musicSource = cam.GetComponent<AudioSource>();

            max_Speed = data.forwardSpeed;
            max_TurnSpeed = data.turnSpeed;
        }


        private void Start()
        {
            Invoke("PlayMusic", 2f);

            s_Idle = createAudioSource(c_Idle, true);
            s_Acceleration = createAudioSource(c_Acceleration, true);
            s_Movement = createAudioSource(c_Movement, true);
            s_Turbine = createAudioSource(c_Turbine, true);
            s_Start = createAudioSource(c_Start, false);
            s_Stop = createAudioSource(c_Stop, false);
            s_ObstacleHit = createAudioSource(c_ObstacleHit[0], false);

            timerHitSound = Random.Range(1f, 6f);
        }


        private AudioSource createAudioSource(AudioClip clip, bool isLooped)
        {
            GameObject newSoundObj = Instantiate(audioSourcePrefab);
            newSoundObj.transform.SetParent(this.transform);
            newSoundObj.transform.localPosition = Vector3.zero;
            newSoundObj.name = clip.name + " Sound";
            AudioSource aSource = newSoundObj.GetComponent<AudioSource>();
            aSource.clip = clip;
            if (isLooped)
                aSource.Play();
            else
                aSource.loop = false;
            return aSource;
        }

        private void Update()
        {
            acceleration = (Input.GetKey(KeyCode.W));
            deceleration = (Input.GetKey(KeyCode.S));

            turnInputValue = Input.GetAxis(turnAxisName);
            movementInputValue = Input.GetAxis(movementAxisName);

            // FadeOut
            if (fadeOut)
            {
                fadeOutSource.volume *= 0.95f;

                fadeOutTimer -= Time.deltaTime;

                if (fadeOutTimer < 0f && fadeOutSource.volume < 0.01f)
                {
                    fadeOut = false;
                    fadeOutTimer = fadeOutTime;
                    fadeOutSource.Stop();
                    fadeOutSource = null;
                }
            }
        }


        private void FixedUpdate()
        {
            Move();         

            CalculateVariables();

            Magic();
        }

        private void Magic()
        {
            // volume & pitch
            if (s_Idle)
            {
                float gain = 0.13f * rpm + 0.65f;
                s_Idle.volume = gain * volume_Idle;
                s_Idle.pitch = 0.65f * rpm + 0.85f;
            }
            if (s_Acceleration)
            {
                float gain = (0.2f * rpm + 0.2f) * accelerationFactor;
                s_Acceleration.volume = gain * volume_Acceleration;
                s_Acceleration.pitch = 0.65f * rpm + 0.75f;
            }
            if (s_Movement)
            {
                float gain = Mathf.Pow(normSpeed, 0.22f);
                float turnGain = Mathf.Pow(turnInputValue, 0.22f);
                s_Movement.volume = Mathf.Min(1f,(gain + 0.6f * Mathf.Abs(turnInputValue)) * volume_Movement);
                s_Movement.pitch = 1.1f * normSpeed + 0.6f;
            }
            if (s_Turbine)
            {
                float gain = 0.4f + 0.6f * turbineRPM;
                s_Turbine.volume = gain * volume_Turbine;
                s_Turbine.pitch = 0.34f * turbineRPM + 0.66f;
            }
            if (s_Start && fadeOutSource != s_Start)
                s_Start.volume = volume_Start;
            if (s_Stop && fadeOutSource != s_Stop)
                s_Stop.volume = volume_Stop;

            if (s_ObstacleHit)
                s_ObstacleHit.volume = volume_ObstacleHit;

            // Start & Stop sounds
            if (acceleration != accelerationPrev)
            {
                if (acceleration)
                {
                    s_Start.Play();
                    if (s_Stop.isPlaying)
                        FadeOut(s_Stop);
                }
                else
                {
                    s_Stop.Play();
                    if (s_Start.isPlaying)
                        FadeOut(s_Start);
                }
            }
            accelerationPrev = acceleration;

            //Obstacle Hit Sounds
            if (timerHitSound < 0f)
            {
                s_ObstacleHit.clip = c_ObstacleHit[Random.Range(0, c_ObstacleHit.Count - 1)];

                if (normSpeed > 0.23f)
                    s_ObstacleHit.Play();
                timerHitSound = Random.Range(1f, 6f);
            }
        }

        private void FadeOut(AudioSource aSource)
        {
            fadeOutSource = aSource;
            fadeOut = true;
        }

        private void CalculateVariables()
        {
            speed = characterController.velocity.magnitude;

            rpm = (speed > 30f) ? 2.333E-03f * speed + 0.86f : -1.033E-03f * speed * speed + 0.062f * speed;

            turbineRPM += (rpm - turbineRPM) * 0.02f;

            normSpeed = speed / max_Speed;

            if (acceleration)
                accelerationFactor = 1f + 1.5f * Mathf.Pow(Mathf.Max(0f, movementInputValue), 0.1f);
            else
                accelerationFactor *= 0.96f;

            timerHitSound -= Time.deltaTime;
        }

        private void Move()
        {
            float movementValue = 0f;

            if (acceleration)
            {
                movementValue = 1f;
            }
            if (deceleration)
            {
                movementValue = -0.2f;
            }

            movementValue -= Mathf.Abs(turnInputValue);

            movementValue = Mathf.Max(0f, prevMovementValue + (movementValue - prevMovementValue) * 0.01f);

            movement = transform.forward * movementValue * movementValue * max_Speed * Time.deltaTime;

            characterController.SimpleMove(movement);

            prevMovementValue = movementValue;
        }

        private void PlayMusic()
        {
            if (musicSource)
                musicSource.Play();
        }
    }
}